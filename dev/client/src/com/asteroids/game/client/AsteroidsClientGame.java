package com.asteroids.game.client;

import com.asteroids.game.client.connection.Client;
import com.asteroids.game.client.connection.PingWatcher;
import com.asteroids.game.client.connection.synchronization.LocalStateSynchronizer;
import com.asteroids.game.client.connection.SocketIoClient;
import com.asteroids.game.client.controls.KeyboardControls;
import com.asteroids.game.util.Delay;
import com.asteroids.game.container.BulletsContainer;
import com.asteroids.game.container.Container;
import com.asteroids.game.container.PlayersContainer;
import com.asteroids.game.controls.Controls;
import com.asteroids.game.manager.Collider;
import com.asteroids.game.model.Arena;
import com.asteroids.game.model.Bullet;
import com.asteroids.game.model.Player;
import com.asteroids.game.rendering.ContainerRenderer;
import com.asteroids.game.rendering.PlayerRenderer;
import com.asteroids.game.rendering.VisibleRenderer;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Map;

import static com.asteroids.game.AsteroidsGame.*;

public class AsteroidsClientGame extends Game {
    private Screen asteroids;

    @Override
    public void create() {
        Viewport viewport = new FillViewport(WORLD_WIDTH, WORLD_HEIGHT);
        ShapeRenderer shapeRenderer =  new ShapeRenderer();
        Controls localControls = new KeyboardControls();

        Arena arena = new Arena(WORLD_WIDTH, WORLD_HEIGHT);
        Container<Bullet> othersBulletsContainer = new BulletsContainer();
        Container<Bullet> localPlayerBulletsContainer = new BulletsContainer();
        Container<Player> playersContainer = new PlayersContainer<>();
        Collider<Player> localPlayerBulletsCollider = new Collider<>(playersContainer, localPlayerBulletsContainer);

        ContainerRenderer<Bullet> othersBulletsRenderer = new ContainerRenderer<>(othersBulletsContainer, VisibleRenderer::new);
        ContainerRenderer<Bullet> localPlayerBulletsRenderer = new ContainerRenderer<>(localPlayerBulletsContainer, VisibleRenderer::new);
        ContainerRenderer<Player> playersRenderer = new ContainerRenderer<>(playersContainer, PlayerRenderer::new);

        Map<String, String> env = System.getenv();
        String protocol = env.getOrDefault("PROTOCOL", "http");
        String host = env.getOrDefault("HOST", "localhost");
        int port = Integer.parseInt(env.getOrDefault("PORT", "8080"));
        LocalStateSynchronizer localStateSynchronizer = new LocalStateSynchronizer();
        Client client = new SocketIoClient(protocol, host, port, localStateSynchronizer, new PingWatcher(), new Delay(0));

        asteroids = new AsteroidsClientScreen(
                localControls, client, localStateSynchronizer,
                viewport, shapeRenderer, arena,
                playersContainer, othersBulletsContainer, localPlayerBulletsContainer,
                localPlayerBulletsCollider,
                playersRenderer, othersBulletsRenderer, localPlayerBulletsRenderer);

        setScreen(asteroids);
    }

    @Override
    public void dispose() {
        asteroids.dispose();
    }
}
