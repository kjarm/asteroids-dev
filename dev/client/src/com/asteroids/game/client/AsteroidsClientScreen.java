package com.asteroids.game.client;

import com.asteroids.game.client.connection.Client;
import com.asteroids.game.client.connection.synchronization.LocalStateSynchronizer;
import com.asteroids.game.controls.NoopControls;
import com.asteroids.game.util.Delay;
import com.asteroids.game.container.Container;
import com.asteroids.game.controls.Controls;
import com.asteroids.game.dto.BulletDto;
import com.asteroids.game.dto.GameStateDto;
import com.asteroids.game.dto.PlayerDto;
import com.asteroids.game.dto.mapper.BulletMapper;
import com.asteroids.game.dto.mapper.ControlsMapper;
import com.asteroids.game.dto.mapper.GameStateMapper;
import com.asteroids.game.dto.mapper.PlayerMapper;
import com.asteroids.game.manager.Collider;
import com.asteroids.game.model.Arena;
import com.asteroids.game.model.Bullet;
import com.asteroids.game.model.Player;
import com.asteroids.game.model.Ship;
import com.asteroids.game.rendering.ContainerRenderer;
import com.asteroids.game.util.Randomize;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class AsteroidsClientScreen extends ScreenAdapter {
    private final Controls localControls;
    private final Client client;
    private final LocalStateSynchronizer localStateSynchronizer;
    private final Viewport viewport;
    private final ShapeRenderer shapeRenderer;
    private final Arena arena;
    private final Collider<Player> localPlayerBulletsCollider;
    private final Container<Player> playersContainer;
    private final Container<Bullet> othersBulletsContainer;
    private final Container<Bullet> localPlayerBulletsContainer;
    private final ContainerRenderer<Player> playersRenderer;
    private final ContainerRenderer<Bullet> othersBulletsRenderer;
    private final ContainerRenderer<Bullet> localPlayerBulletsRenderer;
    private Player localPlayer;

    public AsteroidsClientScreen(
            Controls localControls, Client client, LocalStateSynchronizer localStateSynchronizer,
            Viewport viewport, ShapeRenderer shapeRenderer, Arena arena,
            Container<Player> playersContainer, Container<Bullet> othersBulletsContainer, Container<Bullet> localPlayerBulletsContainer,
            Collider<Player> localPlayerBulletsCollider,
            ContainerRenderer<Player> playersRenderer,
            ContainerRenderer<Bullet> othersBulletsRenderer, ContainerRenderer<Bullet> localPlayerBulletsRenderer) {
        this.localControls = localControls;
        this.client = client;
        this.localStateSynchronizer = localStateSynchronizer;
        this.viewport = viewport;
        this.arena = arena;
        this.localPlayerBulletsCollider = localPlayerBulletsCollider;
        this.playersContainer = playersContainer;
        this.othersBulletsContainer = othersBulletsContainer;
        this.localPlayerBulletsContainer = localPlayerBulletsContainer;
        this.shapeRenderer = shapeRenderer;
        this.playersRenderer = playersRenderer;
        this.othersBulletsRenderer = othersBulletsRenderer;
        this.localPlayerBulletsRenderer = localPlayerBulletsRenderer;
    }

    @Override
    public void show() {
        localPlayerBulletsCollider.onCollision(((player, bullet) -> bullet.noticeHit()));

        client.onConnected(introductoryStateDto -> {
            localPlayer = PlayerMapper.localPlayerFromDto(introductoryStateDto.getConnected(), localControls);
            localStateSynchronizer.setLocalPlayer(localPlayer);
            playersContainer.add(localPlayer);

            GameStateDto gameStateDto = introductoryStateDto.getGameState();
            gameStateDto.getPlayers().stream()
                    .map(playerDto -> PlayerMapper.localPlayerFromDto(playerDto, new NoopControls()))
                    .forEach(playersContainer::add);

            gameStateDto.getBullets().stream()
                    .map(bulletDto -> BulletMapper.fromDto(bulletDto, playersContainer))
                    .forEach(othersBulletsContainer::add);
        });

        client.onOtherPlayerConnected(connectedDto -> {
            Player connected = PlayerMapper.localPlayerFromDto(connectedDto, new NoopControls());
            playersContainer.add(connected);
        });

        client.onOtherPlayerDisconnected(playersContainer::removeById);

        client.onGameStateReceived(gameStateDto -> {
            gameStateDto.getBullets().stream()
                    .filter(bulletDto -> !othersBulletsContainer.getById(bulletDto.getId()).isPresent())
                    .filter(bulletDto -> !bulletDto.getShooterId().equals(localPlayer.getId().toString()))
                    .forEach(bulletDto -> {
                        othersBulletsContainer.add(BulletMapper.fromDto(bulletDto, playersContainer));
                    });

            List<String> existingBulletIds = gameStateDto.getBullets().stream()
                    .map(BulletDto::getId)
                    .collect(toList());

            othersBulletsContainer.getAll().stream()
                    .map(Bullet::getId)
                    .map(Object::toString)
                    .filter(id -> !existingBulletIds.contains(id))
                    .collect(toList())
                    .forEach(othersBulletsContainer::removeById);
        });

        localStateSynchronizer.updateAccordingToGameState(gameStateDto -> {
            gameStateDto.getPlayers().stream()
                    .forEach(playerDto -> playersContainer
                            .getById(playerDto.getId())
                            .ifPresent(player -> PlayerMapper.updateByDto(player, playerDto)));

            gameStateDto.getBullets().stream()
                    .filter(bulletDto -> othersBulletsContainer.getById(bulletDto.getId()).isPresent())
                    .forEach(bulletDto ->
                            BulletMapper.updateByDto(othersBulletsContainer.getById(bulletDto.getId()).get(), bulletDto));
        });

        localStateSynchronizer.supplyGameState(() -> GameStateMapper.fromState(playersContainer, othersBulletsContainer));

        localStateSynchronizer.runGameLogic(this::runGameLogic);

        client.connect(new PlayerDto(null, Randomize.fromList(Player.POSSIBLE_COLORS).toString(), null));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(!client.isConnected()) return;
        client.lockEventHandlers();

        client.sendControls(ControlsMapper.mapToDto(localControls));
        runGameLogic(delta);
        conductLocalShooting(delta);
        localStateSynchronizer.recordState(delta,
                ControlsMapper.mapToDto(localControls));

        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        playersRenderer.render(shapeRenderer);
        othersBulletsRenderer.render(shapeRenderer);
        localPlayerBulletsRenderer.render(shapeRenderer);
        shapeRenderer.end();
        client.unlockEventHandlers();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }

    private void runGameLogic(float delta) {
        playersContainer.getAll().stream()
                .map(Player::getShip)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(arena::ensurePlacementWithinBounds);
        playersContainer.move(delta);
        othersBulletsContainer.move(delta);
    }

    private void conductLocalShooting(float delta) {
        localPlayer.update(delta);
        localPlayer.getShip()
                .flatMap(Ship::obtainBullet)
                .ifPresent(bullet -> Delay.execute(
                                () -> localPlayerBulletsContainer.add(bullet),
                                client.getPing() / 3)
                );
        localPlayerBulletsContainer.update(delta);
        localPlayerBulletsContainer.move(delta);
        localPlayerBulletsContainer.getAll().stream()
                .forEach(arena::ensurePlacementWithinBounds);
        localPlayerBulletsCollider.checkCollisions();
    }
}
