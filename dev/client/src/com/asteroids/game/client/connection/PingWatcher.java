package com.asteroids.game.client.connection;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class PingWatcher {
    private final Map<Long, Instant> gameStateIndexAtTime;
    private long lastPing;

    public PingWatcher(Map<Long, Instant> gameStateIndexAtTime) {
        this.gameStateIndexAtTime = gameStateIndexAtTime;
    }

    public PingWatcher() {
        this(new HashMap<>());
    }

    public void acknowledgeSendForIndex(long index) {
        gameStateIndexAtTime.put(index, Instant.now());
    }

    public void acknowledgeReturnForIndex(long index) {
        Instant sentAt = gameStateIndexAtTime.get(index);
        lastPing = Instant.now().toEpochMilli() - sentAt.toEpochMilli();
    }

    public long getPing() {
        return lastPing;
    }
}
