package com.asteroids.game.client.connection;

import com.asteroids.game.dto.ControlsDto;
import com.asteroids.game.dto.GameStateDto;
import com.asteroids.game.dto.IntroductoryStateDto;
import com.asteroids.game.dto.PlayerDto;

import java.util.function.Consumer;

public interface Client {
    void connect(PlayerDto playerDto);
    void onConnected(Consumer<IntroductoryStateDto> handler);
    void onOtherPlayerConnected(Consumer<PlayerDto> handler);
    void onOtherPlayerDisconnected(Consumer<String> handler);
    void onGameStateReceived(Consumer<GameStateDto> handler);
    void sendControls(ControlsDto controlsDto);
    void lockEventHandlers();
    void unlockEventHandlers();
    boolean isConnected();
    long getPing();
}
