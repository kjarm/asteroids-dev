package com.asteroids.game.server;

import com.asteroids.game.AsteroidsGame;
import com.asteroids.game.container.BulletsContainer;
import com.asteroids.game.container.Container;
import com.asteroids.game.dto.GameStateDto;
import com.asteroids.game.dto.PlayerDto;
import com.asteroids.game.dto.mapper.ControlsMapper;
import com.asteroids.game.dto.mapper.GameStateMapper;
import com.asteroids.game.dto.mapper.PlayerMapper;
import com.asteroids.game.manager.Collider;
import com.asteroids.game.manager.Respawner;
import com.asteroids.game.model.*;
import com.asteroids.game.rendering.ContainerRenderer;
import com.asteroids.game.rendering.PlayerRenderer;
import com.asteroids.game.rendering.VisibleRenderer;
import com.asteroids.game.server.connection.Server;
import com.asteroids.game.controls.RemoteControls;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public class AsteroidsServerScreen extends ScreenAdapter {
    private final Server server;
    private final Container<RemotePlayer> playersContainer;
    private final BulletsContainer bulletsContainer;
    private final Arena arena;
    private final Respawner respawner;
    private final Collider<RemotePlayer> collider;

    private final ShapeRenderer shapeRenderer;
    private final ContainerRenderer<RemotePlayer> playersRenderer;
    private final ContainerRenderer<Bullet> bulletsRenderer;
    private final Viewport viewport;

    public AsteroidsServerScreen(Server server,
                                Container<RemotePlayer> playersContainer, BulletsContainer bulletsContainer,
                                Arena arena, Respawner respawner, Collider<RemotePlayer> collider) {
        this.server = server;
        this.playersContainer = playersContainer;
        this.bulletsContainer = bulletsContainer;
        this.arena = arena;
        this.respawner = respawner;
        this.collider = collider;

        viewport = new FillViewport(AsteroidsGame.WORLD_WIDTH, AsteroidsGame.WORLD_HEIGHT);
        shapeRenderer = new ShapeRenderer();
        bulletsRenderer = new ContainerRenderer<>(bulletsContainer, VisibleRenderer::new);
        playersRenderer = new ContainerRenderer<>(playersContainer, PlayerRenderer::new);
    }

    @Override
    public void show() {
        collider.onCollision((player, bullet) -> {
            bullet.noticeHit();
            player.noticeHit();
        });

        server.onPlayerConnected(playerDto -> {
            RemotePlayer connected = PlayerMapper.remotePlayerFromDto(playerDto);
            respawner.respawnFor(connected);
            PlayerDto connectedDto = PlayerMapper.fromPlayer(connected);
            GameStateDto gameStateDto = GameStateMapper.fromState(playersContainer, bulletsContainer);

            server.sendIntroductoryDataToConnected(connectedDto, gameStateDto);
            server.notifyOtherPlayersAboutConnected(connectedDto);
            playersContainer.add(connected);
        });

        server.onPlayerDisconnected(id -> {
            playersContainer.removeById(id);
            bulletsContainer.removeByPlayerId(id);
        });

        server.onPlayerSentControls((id, controlsDto) -> {
            playersContainer
                    .getById(id)
                    .ifPresent(sender -> ControlsMapper
                            .setRemoteControlsByDto(controlsDto, sender.getRemoteControls()));
        });

        server.start();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        respawner.respawn();
        collider.checkCollisions();

        playersContainer.update(delta);
        playersContainer.move(delta);
        existingShipsStream()
                .forEach(arena::ensurePlacementWithinBounds);
        existingShipsStream()
                .map(Ship::obtainBullet)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(bulletsContainer::add);

        bulletsContainer.update(delta);
        bulletsContainer.move(delta);
        bulletsContainer.getAll().stream()
                .forEach(arena::ensurePlacementWithinBounds);

        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        playersRenderer.render(shapeRenderer);
        bulletsRenderer.render(shapeRenderer);
        shapeRenderer.end();

        server.broadcast(GameStateMapper.fromState(playersContainer, bulletsContainer));
    }

    private Stream<Ship> existingShipsStream() {
        return playersContainer.getAll().stream()
                .map(Player::getShip)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }
}
