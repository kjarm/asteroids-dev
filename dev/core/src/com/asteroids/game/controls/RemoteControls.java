package com.asteroids.game.controls;

import com.asteroids.game.controls.Controls;

public class RemoteControls implements Controls {
    private boolean forward;
    private boolean left;
    private boolean right;
    private boolean shoot;

    @Override
    public boolean forward() {
        return forward;
    }

    @Override
    public boolean left() {
        return left;
    }

    @Override
    public boolean right() {
        return right;
    }

    @Override
    public boolean shoot() {
        return shoot;
    }

    public void forward(boolean state) {
        forward = state;
    }

    public void left(boolean state) {
        left = state;
    }

    public void right(boolean state) {
        right = state;
    }

    public void shoot(boolean state) {
        shoot = state;
    }
}
