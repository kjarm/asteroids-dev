package com.asteroids.game.container;

import com.asteroids.game.model.Player;

import java.util.ArrayList;
import java.util.List;


public class PlayersContainer<PlayerType extends Player> implements Container<PlayerType> {
    private final List<PlayerType> players;

    public PlayersContainer(List<PlayerType> players) {
        this.players = players;
    }

    public PlayersContainer() {
        this(new ArrayList<>());
    }

    @Override
    public void add(PlayerType toAdd) {
        players.add(toAdd);
    }

    @Override
    public List<PlayerType> getAll() {
        return players;
    }

    @Override
    public void update(float delta) {
        players.forEach(player -> player.update(delta));
    }

    @Override
    public void move(float delta) {
        players.forEach(player -> player.move(delta));
    }
}
