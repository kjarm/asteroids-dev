package com.asteroids.game.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;

public class IndexedGameStateDto extends IndexedDto<GameStateDto> {
    public IndexedGameStateDto(
            @JsonProperty("dto") GameStateDto dto,
            @JsonProperty("index") long index) {
        super(dto, index);
    }


    public static IndexedGameStateDto fromJsonString(String json) {
        try {
            return objectMapper.readValue(json, IndexedGameStateDto.class);
        } catch (IOException e) {
            throw new RuntimeException("Error while creating IndexedGameStateDto from JSON", e);
        }
    }
}
