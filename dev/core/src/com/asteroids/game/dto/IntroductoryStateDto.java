package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class IntroductoryStateDto implements Dto {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final PlayerDto connected;
    private final GameStateDto gameState;

    @JsonCreator
    public IntroductoryStateDto(
            @JsonProperty("connected") PlayerDto connected,
            @JsonProperty("gameState") GameStateDto gameState) {
        this.connected = connected;
        this.gameState = gameState;
    }

    @Override
    public String toJsonString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting IntroductoryStateDto to JSON", e);
        }
    }

    public static IntroductoryStateDto fromJsonString(String json) {
        try {
            return objectMapper.readValue(json, IntroductoryStateDto.class);
        } catch (IOException e) {
            throw new RuntimeException("Error while creating IntroductoryStateDto from JSON", e);
        }
    }

    public PlayerDto getConnected() {
        return connected;
    }

    public GameStateDto getGameState() {
        return gameState;
    }
}
