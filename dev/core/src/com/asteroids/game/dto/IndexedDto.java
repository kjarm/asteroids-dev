package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IndexedDto<UnderlyingDto extends Dto> implements Dto {
    protected static final ObjectMapper objectMapper = new ObjectMapper();
    private final UnderlyingDto dto;
    private final long index;

    public IndexedDto(
            @JsonProperty("dto") UnderlyingDto dto,
            @JsonProperty("index") long index) {
        this.dto = dto;
        this.index = index;
    }

    @Override
    public String toJsonString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting IndexedDto to JSON", e);
        }
    }

    public UnderlyingDto getDto() {
        return dto;
    }

    public long getIndex() {
        return index;
    }
}
