package com.asteroids.game.dto;

public interface Dto {
    String toJsonString();
}
