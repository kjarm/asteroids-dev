package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;

public class IndexedControlsDto extends IndexedDto<ControlsDto> {
    public IndexedControlsDto(
            @JsonProperty("dto") ControlsDto dto,
            @JsonProperty("index") long index) {
        super(dto, index);
    }

    public static IndexedControlsDto fromJsonString(String json) {
        try {
            return objectMapper.readValue(json, IndexedControlsDto.class);
        } catch (IOException e) {
            throw new RuntimeException("Error while creating IndexedControlsDto from JSON", e);
        }
    }
}
