package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ShipDto implements Dto {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final float x;
    private final float y;
    private final float rotation;
    private final float velocityX;
    private final float velocityY;
    private final float rotationVelocity;

    @JsonCreator
    public ShipDto(
            @JsonProperty("x") float x,
            @JsonProperty("y") float y,
            @JsonProperty("rotation") float rotation,
            @JsonProperty("velocityX") float velocityX,
            @JsonProperty("velocityY") float velocityY,
            @JsonProperty("rotationVelocity") float rotationVelocity) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.rotationVelocity = rotationVelocity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShipDto shipDto = (ShipDto) o;

        if (Float.compare(shipDto.x, x) != 0) return false;
        if (Float.compare(shipDto.y, y) != 0) return false;
        return Float.compare(shipDto.rotation, rotation) == 0;
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (rotation != +0.0f ? Float.floatToIntBits(rotation) : 0);
        return result;
    }

    @Override
    public String toJsonString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting ShipDto to JSON", e);
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getRotation() {
        return rotation;
    }

    public float getVelocityX() {
        return velocityX;
    }

    public float getVelocityY() {
        return velocityY;
    }

    public float getRotationVelocity() {
        return rotationVelocity;
    }
}
