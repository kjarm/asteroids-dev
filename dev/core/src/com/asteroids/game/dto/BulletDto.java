package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class BulletDto implements Dto {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final String id;
    private final float x;
    private final float y;
    private final float rotation;
    private final String shooterId;

    @JsonCreator
    public BulletDto(
            @JsonProperty("id") String id,
            @JsonProperty("x") float x,
            @JsonProperty("y") float y,
            @JsonProperty("rotation") float rotation,
            @JsonProperty("shooterId") String shooterId) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.shooterId = shooterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BulletDto bulletDto = (BulletDto) o;

        if (Float.compare(bulletDto.x, x) != 0) return false;
        if (Float.compare(bulletDto.y, y) != 0) return false;
        if (Float.compare(bulletDto.rotation, rotation) != 0) return false;
        if (!id.equals(bulletDto.id)) return false;
        return shooterId.equals(bulletDto.shooterId);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (rotation != +0.0f ? Float.floatToIntBits(rotation) : 0);
        result = 31 * result + shooterId.hashCode();
        return result;
    }

    @Override
    public String toJsonString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting BulletDto to JSON", e);
        }
    }

    public String getId() {
        return id;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getRotation() {
        return rotation;
    }

    public String getShooterId() {
        return shooterId;
    }
}
