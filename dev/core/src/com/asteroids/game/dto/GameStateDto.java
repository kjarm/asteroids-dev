package com.asteroids.game.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class GameStateDto implements Dto {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final List<PlayerDto> players;
    private final List<BulletDto> bullets;

    @JsonCreator
    public GameStateDto(
            @JsonProperty("players") List<PlayerDto> players,
            @JsonProperty("bullets") List<BulletDto> bullets) {
        this.players = players;
        this.bullets = bullets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameStateDto that = (GameStateDto) o;

        if (!players.equals(that.players)) return false;
        return bullets.equals(that.bullets);

    }

    @Override
    public int hashCode() {
        int result = players.hashCode();
        result = 31 * result + bullets.hashCode();
        return result;
    }

    @Override
    public String toJsonString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error while converting GameStateDto to JSON", e);
        }
    }

    public static GameStateDto fromJsonString(String json) {
        try {
            return objectMapper.readValue(json, GameStateDto.class);
        } catch (IOException e) {
            throw new RuntimeException("Error while creating GameStateDto from JSON", e);
        }
    }

    public List<PlayerDto> getPlayers() {
        return players;
    }

    public List<BulletDto> getBullets() {
        return bullets;
    }
}
