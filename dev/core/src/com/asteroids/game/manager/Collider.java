package com.asteroids.game.manager;

import com.asteroids.game.container.Container;
import com.asteroids.game.model.Bullet;
import com.asteroids.game.model.Player;

import java.util.function.BiConsumer;

public class Collider<PlayerType extends Player> {
    private final Container<PlayerType> playersContainer;
    private final Container<Bullet> bulletsContainer;
    private BiConsumer<PlayerType, Bullet> collisionHandler;

    public Collider(Container<PlayerType> playersContainer, Container<Bullet> bulletsContainer) {
        this.playersContainer = playersContainer;
        this.bulletsContainer = bulletsContainer;
    }

    public void onCollision(BiConsumer<PlayerType, Bullet> handler) {
        collisionHandler = handler;
    }

    public void checkCollisions() {
        bulletsContainer.getAll().stream()
                .forEach(bullet -> playersContainer.getAll().stream()
                        .filter(player -> player.getShip().isPresent())
                        .filter(player -> player.getShip().get().collidesWith(bullet) && !player.getId().equals(bullet.getShooterId()))
                        .findFirst()
                        .ifPresent(player -> collisionHandler.accept(player, bullet)));
    }
}
