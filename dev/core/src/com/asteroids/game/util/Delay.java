package com.asteroids.game.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Delay {
    private static final Timer SHARED_TIMER = new Timer(true);
    private final long amount;
    private final Timer timer;

    public Delay(long amount) {
        this(amount, new Timer(true));
    }

    public Delay(long amount, Timer timer) {
        this.amount = amount;
        this.timer = timer;
    }

    public void execute(Runnable task) {
        execute(task, amount, timer);
    }

    public static void execute(Runnable task, long amount) {
        execute(task, amount, SHARED_TIMER);
    }

    private static void execute(Runnable task, long amount, Timer timer) {
        if(amount == 0) {
            task.run();
            return;
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                task.run();
            }
        }, amount);
    }
}
